class SiteController < ApplicationController
  def index
    #We are going to return all the books marks in decending order of time created
    @bookmarks = Bookmark.order('created_at desc')
  end
end

class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :rememberable, :validatable

  #Adding accoication for Users to bookmarks
  has_many :bookmarks
end